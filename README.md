# NestJS Safe Config

A helper for creating NestJS config module with validation and type-safe property access.

## Overview

A config class is required. With the config class, the plain config value (typically loaded from file) is transformed by `class-transformer` and validated by `class-validator`. The config class instance is provided by using its class as the injection token.

## Getting Started

This package is designed to be used with NestJS. Other than NestJS dependencies, `class-transformer` and `class-validator` are also peer dependencies.

### Npm

```
npm install --save nestjs-safe-config class-transformer class-validator
```

### Yarn

```
yarn add nestjs-safe-config class-transformer class-validator
```

## Config Module

This package provides a ConfigModule class to create dynamic module to serve the config.

```ts
class ConfigModule {
	static register(options?: ConfigModuleOptions): DynamicModule;
}

interface ConfigModuleOptions {
	imports?: ModuleMetadata['imports'];
	providers?: ModuleMetadata['providers'];
	global?: boolean;
	configOptions?: CreateConfigProviderOptions<Record<string, any>>[];
}

type CreateConfigProviderOptions<T> =
	| CreateConfigValueProviderOptions<T>
	| CreateConfigFactoryProviderOptions<T>;

interface CreateConfigValueProviderOptions<T>
	extends TransformAndValidateConfigOptions {
	/**
	 * Config class.
	 */
	cls: Type<T>;
	/**
	 * Plain config object.
	 */
	value: Record<string, unknown>;
}

interface CreateConfigFactoryProviderOptions<T>
	extends TransformAndValidateConfigOptions {
	/**
	 * Config class.
	 */
	cls: Type<T>;
	/**
	 * Factory to return plain config object. Accept promise.
	 */
	factory: (
		...args: any[]
	) => Record<string, unknown> | Promise<Record<string, unknown>>;
	/**
	 */
	inject?: (Type<any> | string | symbol | Abstract<any> | Function)[];
	/**
	 * Optional enum defining lifetime of the provider that is returned by the Factory function.
	 */
	scope?: Scope;
}

interface TransformAndValidateConfigOptions {
	/**
	 * Options passed to plainToClass of class-transformer.
	 */
	classTransformerOptions?: ClassTransformOptions;
	/**
	 * Options passed to validate of class-validator.
	 * Provided options will be merged with default options.
	 * Default: { whitelist: true }
	 */
	classValidatorOptions?: ValidatorOptions;
}
```

## Load Utility

This package also provides load utilities for loading different types of config file.
Note that the loaded config value is plain object and not validated.
Also, other peer dependencies may be required by the load utilities.

### JSON

```ts
/**
 * Load json config file with JSON.parse.
 * @param options - Options for loadJsonConfig.
 * @returns Plain config object.
 */
function loadJsonConfig(
	options: LoadJsonConfigOptions
): Record<string, unknown>;

interface LoadJsonConfigOptions {
	/**
	 * Path to json config file.
	 */
	path: string;
}
```

### Yaml

```ts
/**
 * Load dotenv config file with "js-yaml" package.
 * @remark
 * Please make sure "js-yaml" package is installed.
 * @param options - Options for loadYamlConfig.
 * @returns Plain config object.
 */
function loadYamlConfig(
	options: LoadYamlConfigOptions
): Record<string, unknown>;

interface LoadYamlConfigOptions {
	/**
	 * Path to yaml config file.
	 */
	path: string;
}
```

### Dotenv

```ts
/**
 * Load dotenv config file with "dotenv" package.
 * @remark
 * Please make sure "dotenv" package is installed.
 * If you want to use the expandDotEnv option, please also make sure "dotenv-expand" package is installed.
 * @param options - Options for loadDotenvConfig.
 * @return Plain config object.
 */
function loadDotenvConfig(
	options: LoadDotenvConfigOptions
): Record<string, unknown>;

interface LoadDotenvConfigOptions {
	/**
	 * Path to dotenv config file.
	 */
	path: string;
	/**
	 * If true, expand variables in dotenv config with "dotenv-expand" package. Default: false.
	 */
	expandDotenv?: boolean;
	/**
	 * If true, merge dotenv config with process.env. process.env overrides dotenv config. Default: false.
	 */
	mergeProcessEnv?: boolean;
	/**
	 * If true, assign dotenv config to process.env. Default: false.
	 */
	assignProcessEnv?: boolean;
	/**
	 * If provided, it will be used to transform key of dotEnv config.
	 */
	transformEnvKey?: (key: string) => string;
}
```

## Example

Examples can be found in [here](https://gitlab.com/whkwok/nestjs-safe-config/-/tree/master/test/e2e).
