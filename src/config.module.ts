import { DynamicModule, Module, ModuleMetadata } from '@nestjs/common';

import {
	CreateConfigProviderOptions,
	createConfigProvider,
} from './create-config-provider';

export interface ConfigModuleOptions {
	imports?: ModuleMetadata['imports'];
	providers?: ModuleMetadata['providers'];
	global?: boolean;
	configOptions?: CreateConfigProviderOptions<Record<string, any>>[];
}

@Module({})
export class ConfigModule {
	static register(options: ConfigModuleOptions = {}): DynamicModule {
		const {
			imports,
			providers = [],
			global = false,
			configOptions = [],
		} = options;
		const configProviders = configOptions.map(createConfigProvider);
		return {
			module: ConfigModule,
			imports,
			providers: [...providers, ...configProviders],
			exports: [...configProviders],
			global,
		};
	}
}
