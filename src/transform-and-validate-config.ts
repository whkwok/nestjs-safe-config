import { Type } from '@nestjs/common';
import { ClassTransformOptions, plainToClass } from 'class-transformer';
import { ValidatorOptions, validateSync, validate } from 'class-validator';

export interface TransformAndValidateConfigOptions {
	/**
	 * Options passed to plainToClass of class-transformer.
	 */
	classTransformerOptions?: ClassTransformOptions;
	/**
	 * Options passed to validate of class-validator.
	 * Provided options will be merged with default options.
	 * Default: { whitelist: true }
	 */
	classValidatorOptions?: ValidatorOptions;
}

const defaultClassValidatorOptions: ValidatorOptions = { whitelist: true };
/**
 * Transform plain config object to class config instance and validate it.
 * @param cls - Config class.
 * @param value - Plain config object.
 * @return Class config instance.
 */
export async function transformAndValidateConfig<
	T extends Record<string, unknown>
>(
	cls: Type<T>,
	value: Record<string, unknown>,
	{
		classTransformerOptions,
		classValidatorOptions,
	}: TransformAndValidateConfigOptions
): Promise<T> {
	const clsConfig = plainToClass(cls, value, classTransformerOptions);
	const errors = await validate(clsConfig, {
		...defaultClassValidatorOptions,
		...classValidatorOptions,
	});
	if (errors.length) throw errors;
	return clsConfig;
}

/**
 * Sync version of {@link transformAndValidateConfig}
 */
export function transformAndValidateConfigSync<
	T extends Record<string, unknown>
>(
	cls: Type<T>,
	value: Record<string, unknown>,
	{
		classTransformerOptions,
		classValidatorOptions,
	}: TransformAndValidateConfigOptions
): T {
	const clsConfig = plainToClass(cls, value, classTransformerOptions);
	const errors = validateSync(clsConfig, {
		...defaultClassValidatorOptions,
		...classValidatorOptions,
	});
	if (errors.length) throw errors;
	return clsConfig;
}
