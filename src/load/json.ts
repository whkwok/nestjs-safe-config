import { readFileSync } from 'fs';

export interface LoadJsonConfigOptions {
	/**
	 * Path to json config file.
	 */
	path: string;
}

/**
 * Load json config file with JSON.parse.
 * @param options - Options for loadJsonConfig.
 * @returns Plain config object.
 */
export function loadJsonConfig({
	path,
}: LoadJsonConfigOptions): Record<string, unknown> {
	const value = JSON.parse(readFileSync(path, 'utf8'));
	if (!value || typeof value !== 'object')
		throw new Error('Json config must be parsed to an object.');
	return value as Record<string, unknown>;
}
