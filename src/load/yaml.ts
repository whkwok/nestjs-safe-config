import { readFileSync } from 'fs';
import type { load as Tload } from 'js-yaml';

export interface LoadYamlConfigOptions {
	/**
	 * Path to yaml config file.
	 */
	path: string;
}

/**
 * Load dotenv config file with "js-yaml" package.
 * @remark
 * Please make sure "js-yaml" package is installed.
 * @param options - Options for loadYamlConfig.
 * @returns Plain config object.
 */
export function loadYamlConfig({
	path,
}: LoadYamlConfigOptions): Record<string, unknown> {
	let load: typeof Tload;
	try {
		// eslint-disable-next-line @typescript-eslint/no-var-requires
		load = require('js-yaml').load;
	} catch {
		throw new Error(
			'Please make sure "js-yaml" is installed when using loadYamlConfig.'
		);
	}
	const value = load(readFileSync(path, 'utf8'));
	if (!value || typeof value !== 'object')
		throw new Error('Yaml config must be parsed to an object.');
	return value as Record<string, unknown>;
}
