import { readFileSync } from 'fs';
import type { parse as Tparse } from 'dotenv';
import type TdotenvExpand from 'dotenv-expand';

export interface LoadDotenvConfigOptions {
	/**
	 * Path to dotenv config file.
	 */
	path: string;
	/**
	 * If true, expand variables in dotenv config with "dotenv-expand" package. Default: false.
	 */
	expandDotenv?: boolean;
	/**
	 * If true, merge dotenv config with process.env. process.env overrides dotenv config. Default: false.
	 */
	mergeProcessEnv?: boolean;
	/**
	 * If true, assign dotenv config to process.env. Default: false.
	 */
	assignProcessEnv?: boolean;
	/**
	 * If provided, it will be used to transform key of dotEnv config.
	 */
	transformEnvKey?: (key: string) => string;
}

/**
 * Load dotenv config file with "dotenv" package.
 * @remark
 * Please make sure "dotenv" package is installed.
 * If you want to use the expandDotEnv option, please also make sure "dotenv-expand" package is installed.
 * @param options - Options for loadDotenvConfig.
 * @return Plain config object.
 */
export function loadDotenvConfig({
	path,
	expandDotenv = false,
	mergeProcessEnv = false,
	assignProcessEnv = false,
	transformEnvKey,
}: LoadDotenvConfigOptions): Record<string, string> {
	let parse: typeof Tparse;
	try {
		// eslint-disable-next-line @typescript-eslint/no-var-requires
		parse = require('dotenv').parse;
	} catch {
		throw new Error(
			'Please make sure "dotenv" is installed when using loadDotenvConfig.'
		);
	}
	let value: Record<string, string> = parse(readFileSync(path));
	if (expandDotenv) {
		let dotenvExpand: typeof TdotenvExpand;
		try {
			// eslint-disable-next-line @typescript-eslint/no-var-requires
			dotenvExpand = require('dotenv-expand');
		} catch {
			throw new Error(
				'Please make sure "dotenv-expand" is installed when using expandDotenv option.'
			);
		}
		value = dotenvExpand({ parsed: value }).parsed!;
	}
	if (mergeProcessEnv)
		value = { ...value, ...(process.env as Record<string, string>) };
	const keys = Object.keys(value);
	if (assignProcessEnv)
		keys.forEach((key) => {
			process.env[key] = value[key];
		});
	if (transformEnvKey) {
		const newValue: Record<string, string> = {};
		keys.forEach((key) => {
			newValue[transformEnvKey(key)] = value[key];
		});
		value = newValue;
	}
	return value;
}
