import {
	Abstract,
	FactoryProvider,
	Scope,
	Type,
	ValueProvider,
} from '@nestjs/common';

import {
	TransformAndValidateConfigOptions,
	transformAndValidateConfig,
	transformAndValidateConfigSync,
} from './transform-and-validate-config';

export interface CreateConfigValueProviderOptions<T>
	extends TransformAndValidateConfigOptions {
	/**
	 * Config class.
	 */
	cls: Type<T>;
	/**
	 * Plain config object.
	 */
	value: Record<string, unknown>;
}
export interface CreateConfigFactoryProviderOptions<T>
	extends TransformAndValidateConfigOptions {
	/**
	 * Config class.
	 */
	cls: Type<T>;
	/**
	 * Factory to return plain config object. Accept promise.
	 * @see {@link FactoryProvider.useFactory}
	 */
	factory: (
		...args: any[]
	) => Record<string, unknown> | Promise<Record<string, unknown>>;
	/**
	 * @see {@link FactoryProvider.inject}
	 */
	inject?: (Type<any> | string | symbol | Abstract<any> | Function)[];
	/**
	 * Optional enum defining lifetime of the provider that is returned by the Factory function.
	 * @see {@link FactoryProvider.scope}
	 */
	scope?: Scope;
}

export type CreateConfigProviderOptions<T> =
	| CreateConfigValueProviderOptions<T>
	| CreateConfigFactoryProviderOptions<T>;

export function createConfigValueProvider<T extends Record<string, unknown>>(
	options: CreateConfigValueProviderOptions<T>
): ValueProvider<T> {
	const { cls, value, ...restOptions } = options;
	return {
		provide: cls,
		useValue: transformAndValidateConfigSync(cls, value, restOptions),
	};
}

export function createConfigFactoryProvider<T extends Record<string, unknown>>(
	options: CreateConfigFactoryProviderOptions<T>
): FactoryProvider<Promise<Record<string, unknown>>> {
	const { cls, factory, inject, scope, ...restOptions } = options;
	return {
		provide: cls,
		useFactory: async (...args: unknown[]) =>
			await transformAndValidateConfig(
				cls,
				await factory(...args),
				restOptions
			),
		inject,
		scope,
	};
}

export function createConfigProvider<T extends Record<string, unknown>>(
	options: CreateConfigProviderOptions<T>
):
	| ValueProvider<Record<string, unknown>>
	| FactoryProvider<Promise<Record<string, unknown>>> {
	if ('value' in options && 'factory' in options)
		throw new Error('Providing both value and factory is not allowed.');
	if ('value' in options) return createConfigValueProvider(options);
	if ('factory' in options) return createConfigFactoryProvider(options);
	throw new Error('Unrecognized options for configProvider.');
}
