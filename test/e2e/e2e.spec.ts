import { resolve } from 'path';
import { Test, TestingModule } from '@nestjs/testing';
import {
	ConfigModule,
	loadDotenvConfig,
	loadJsonConfig,
	loadYamlConfig,
} from 'src';
import { TestConfig } from './test.config';
import { TestModule } from './test.module';
import { TestService } from './test.service';

interface Ref<T> {
	current: T | null;
}

function setUpTestingModule(config: Record<string, unknown>) {
	const moduleRef: Ref<TestingModule> = { current: null };
	beforeAll(async () => {
		const configModule = ConfigModule.register({
			configOptions: [{ cls: TestConfig, value: config }],
			global: true,
		});
		moduleRef.current = await Test.createTestingModule({
			imports: [configModule, TestModule],
		}).compile();
	});
	return moduleRef;
}

describe('e2e', () => {
	describe('Inline Config', () => {
		const config = { name: 'test', num: 122, str: 'abc' };
		const moduleRef = setUpTestingModule(config);

		it('should be able to be get config through TestService', async () => {
			const config = moduleRef.current!.get(TestService).getConfig();
			expect(config).toEqual({ name: 'test', num: 122, str: 'abc' });
		});
	});

	describe('Json Config', () => {
		const configPath = resolve(__dirname, '../test-config.json');
		const config = loadJsonConfig({ path: configPath });
		const moduleRef = setUpTestingModule(config);

		it('should be able to be get config through TestService', async () => {
			const config = moduleRef.current!.get(TestService).getConfig();
			expect(config).toEqual({ name: 'test', num: 122, str: 'abc' });
		});
	});

	describe('Yaml Config', () => {
		const configPath = resolve(__dirname, '../test-config.yaml');
		const config = loadYamlConfig({ path: configPath });
		const moduleRef = setUpTestingModule(config);

		it('should be able to be get config through TestService', async () => {
			const config = moduleRef.current!.get(TestService).getConfig();
			expect(config).toEqual({ name: 'test', num: 122, str: 'abc' });
		});
	});

	describe('DotEnv Config (Config File Only)', () => {
		const originalEnv = process.env;
		process.env['NUM'] = '333';
		const configPath = resolve(__dirname, '../test-config.env');
		const config = loadDotenvConfig({
			path: configPath,
			transformEnvKey: (key) => key.toLowerCase(),
		});
		process.env = originalEnv;
		const moduleRef = setUpTestingModule(config);

		it('should be able to be get config through TestService', async () => {
			const config = moduleRef.current!.get(TestService).getConfig();
			expect(config).toEqual({ name: 'test', num: 122, str: 'abc' });
		});
	});

	describe('DotEnv Config (Merge process.env)', () => {
		const originalEnv = process.env;
		process.env['NUM'] = '333';
		const configPath = resolve(__dirname, '../test-config.env');
		const config = loadDotenvConfig({
			path: configPath,
			transformEnvKey: (key) => key.toLowerCase(),
			mergeProcessEnv: true,
		});
		process.env = originalEnv;
		const moduleRef = setUpTestingModule(config);

		it('should be able to be get config through TestService', async () => {
			const config = moduleRef.current!.get(TestService).getConfig();
			expect(config).toEqual({ name: 'test', num: 333, str: 'abc' });
		});
	});
});
