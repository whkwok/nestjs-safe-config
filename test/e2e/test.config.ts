import { Type } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class TestConfig {
	@IsString()
	name: string = 'default_name';
	@IsNumber()
	@Type(() => Number)
	num: number = 0;
	@IsString()
	str: string = 'a';
}
