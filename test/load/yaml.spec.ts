import { resolve } from 'path';
import { loadYamlConfig } from 'src/load/yaml';

describe('loadYamlConfig', () => {
	it('should be able to load yaml config', async () => {
		const value = loadYamlConfig({
			path: resolve(__dirname, '../test-config.yaml'),
		});
		expect(value).toEqual({ name: 'test', num: 122, str: 'abc' });
	});
});
