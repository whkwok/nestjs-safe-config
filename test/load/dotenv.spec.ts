import { resolve } from 'path';
import { loadDotenvConfig } from 'src/load/dotenv';

describe('loadDotenvConfig', () => {
	const originalEnv = process.env;

	afterEach(() => {
		process.env = originalEnv;
	});

	it('should be able to load dotenv config', async () => {
		const value = loadDotenvConfig({
			path: resolve(__dirname, '../test-config.env'),
		});
		expect(value).toEqual({ NAME: 'test', NUM: '122', STR: 'abc' });
	});

	it('should be able to load dotenv config and transform env key', async () => {
		const value = loadDotenvConfig({
			path: resolve(__dirname, '../test-config.env'),
			transformEnvKey: (key) => key.toLowerCase(),
		});
		expect(value).toEqual({ name: 'test', num: '122', str: 'abc' });
	});

	it('should be able to load dotenv config and assign it to process.env', async () => {
		const value = loadDotenvConfig({
			path: resolve(__dirname, '../test-config.env'),
			assignProcessEnv: true,
		});
		expect(value).toEqual({ NAME: 'test', NUM: '122', STR: 'abc' });
		expect(process.env).toEqual(
			expect.objectContaining({ NAME: 'test', NUM: '122', STR: 'abc' })
		);
	});

	it('should be able to load dotenv config and merge it with process.env', async () => {
		process.env['ORIGINAL'] = 'original';
		process.env['NUM'] = '334';
		const value = loadDotenvConfig({
			path: resolve(__dirname, '../test-config.env'),
			mergeProcessEnv: true,
		});
		expect(value).toEqual(
			expect.objectContaining({
				NAME: 'test',
				NUM: '334',
				STR: 'abc',
				ORIGINAL: 'original',
			})
		);
	});
});
