import { resolve } from 'path';
import { loadJsonConfig } from 'src/load/json';

describe('loadJsonConfig', () => {
	it('should be able to load json config', async () => {
		const value = loadJsonConfig({
			path: resolve(__dirname, '../test-config.json'),
		});
		expect(value).toEqual({ name: 'test', num: 122, str: 'abc' });
	});
});
