import { Test } from '@nestjs/testing';
import { Type } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';
import { ConfigModule } from 'src/config.module';

describe('ConfigModule', () => {
	class TestConfig1 {
		@IsString()
		name: string = 'default_name1';
		@IsNumber()
		@Type(() => Number)
		num: number = 1;
	}

	class TestConfig2 {
		@IsString()
		name: string = 'default_name2';
		@IsNumber()
		@Type(() => Number)
		num: number = 2;
	}

	class TestService {
		async getName() {
			return new Promise<string>((resolve) => {
				setTimeout(() => resolve('testServiceName'), 10);
			});
		}
	}

	it('should be able to be created with no options', async () => {
		await Test.createTestingModule({
			imports: [ConfigModule.register()],
		}).compile();
	});

	it('should be able to be created with configValueOptions', async () => {
		const moduleRef = await Test.createTestingModule({
			imports: [
				ConfigModule.register({
					configOptions: [
						{ cls: TestConfig1, value: { name: 'test_name1', num: '11' } },
						{ cls: TestConfig2, value: {} },
					],
				}),
			],
		}).compile();
		const config1 = moduleRef.get(TestConfig1);
		expect(config1).toBeInstanceOf(TestConfig1);
		expect(config1).toEqual({ name: 'test_name1', num: 11 });
		const config2 = moduleRef.get(TestConfig2);
		expect(config2).toBeInstanceOf(TestConfig2);
		expect(config2).toEqual({ name: 'default_name2', num: 2 });
	});

	it('should be able to be created with configFactoryOptions', async () => {
		const moduleRef = await Test.createTestingModule({
			imports: [
				ConfigModule.register({
					providers: [TestService],
					configOptions: [
						{
							cls: TestConfig1,
							factory: () => ({ name: 'test_name1', num: '11' }),
						},
						{
							cls: TestConfig2,
							factory: async (testService: TestService) => ({
								name: await testService.getName(),
							}),
							inject: [TestService],
						},
					],
				}),
			],
		}).compile();
		const config1 = moduleRef.get(TestConfig1);
		expect(config1).toBeInstanceOf(TestConfig1);
		expect(config1).toEqual({ name: 'test_name1', num: 11 });
		const config2 = moduleRef.get(TestConfig2);
		expect(config2).toBeInstanceOf(TestConfig2);
		expect(config2).toEqual({ name: 'testServiceName', num: 2 });
	});
});
